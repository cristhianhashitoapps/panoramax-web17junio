
<?php
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;
class PedidoController extends ScaffoldController
{

  public $model = 'solicitudexamenes';
  public function index($page=1)
    {
        $this->data = (new Solicitudexamenes)->getsolicitudExamenes($page);
    }


  public function pedidoatendido($Id){

    $solicitudexamenes = new Solicitudexamenes();

    if($Id != null){

        $this->solicitudexamenes = $solicitudexamenes->find((int)$Id);
        $solicitudexamenes->TipoEstadoId = 2;
    }

    if(!$solicitudexamenes->update(Input::post('solicitudexamenes'))){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->solicitudexamenes = Input::post('solicitudexamenes');
            } else {
               return Redirect::to();
            }
  }


  public function detalleexamen($Id)
  {
     $detalle = new Detallesexamenes();
        $this->detalleexamen = $detalle->find("SolicitudExameneId=".$Id,"columns: detallesexamenes.Id, detallesexamenes.campoAbierto,detallesexamenes.createdAt, detallesexamenes.updatedAt, sub.nombre as SubCategoriumId,sub.clase as Clase, detallesexamenes.SolicitudExameneId",'join: left outer join subcategoria sub on detallesexamenes.SubCategoriumId = sub.Id');
  }

  public function imprimir($Id){
    $examen = new Solicitudexamenes();
    $examen = $examen->find_first($Id);
    $doctor = new Doctors();
    $doctor = $doctor->find_first("id=$examen->DoctorId");
    $examen->DoctorNombre = $doctor->nombres;
    $examen->DoctorTelefono = $doctor->telefono;
    $examen->DoctorCorreo = $doctor->correo;
    $paciente = new Pacientes();
    $paciente = $paciente->find_first("id=$examen->PacienteId");
    $examen->PacienteNombre = $paciente->nombres." ".$paciente->apellidos;
    $examen->PacienteEdad = $paciente->edad;
    $examen->PacienteTelefono = $paciente->telefono;
    $examen->PacienteEmail= $paciente->email;
    $examen->PacienteDocumento = $paciente->cedula;
    $examen->PacienteFechaNacimiento = $paciente->fechaNacimiento;

    $empresa = new Empresas();
    $empresa = $empresa->find_first($examen->EmpresaId);
    $empresaNombre = $empresa->tipoDireccion.'-'.$empresa->Num1.'-'.$empresa->Num2.'-'.$empresa->Num3;
    $examen->EmpresaNombre = $empresaNombre;

    $tiposervicio = new Tiposervicios();
    $tiposervicio = $tiposervicio->find_first("id=$examen->TipoServicioId");
    $examen->TipoServicioNombre = $tiposervicio->nombre;

    $tipoentrega = new Tipoentregaresultados();
    $arrtipoentrega = array();
    $arrtipoentrega = $tipoentrega->find_all_by_sql("select t.nombre as Nombre from solicitudtipoentregas s,tipoentregaresultados t
    where s.TipoEntregaResultadoId=t.Id and s.SolicitudExameneId=$Id");


    $detalle = new Detallesexamenes();
    $arr = array();
    $arr = $detalle->find("SolicitudExameneId=$Id",
    "columns: DetallesExamenes.campoabierto as campoAbierto,
    Subcategoria.nombre as SubCategoriumNombre,
    Subcategoria.Id as SubcategoriumId,
    SubCategoria.clase as SubCategoriumClase,
    Categoria.Id as CategoriaId,
    Categoria.nombre as CategoriaNombre
    ",
    "join: inner join Subcategoria on SubcategoriumId=Subcategoria.Id
     inner join Categoria on Subcategoria.CategoriumId=Categoria.Id");

    $arrfinal = array("pedido"=>$examen,"detalles"=>$arr,"tiposolicitudentrega"=>$arrtipoentrega);

    //envio de socket
    $server = VerifySource::getSocketServer();
    $client = new Client(new Version1X($server));
    $client->initialize();

    $client->emit('imprimir-pedido', $arrfinal);

    $client->close();
  }


}
