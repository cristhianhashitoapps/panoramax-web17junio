
<?php
class SubCategoriaController extends ScaffoldController
{
 public $model = 'subcategoria';

  public function index($Id,$page = 1)
    {

    	$cat = new Categoria();
        $this->categoria = $cat->find_first("Id=".$Id,"columns: categoria.Id, categoria.nombre");
        $this->data = (new Subcategoria)->getSubcategoria($Id,$page);
    }


      /**
     * Crea un Registro
     */
    public function crear($Id)
    {

    	$this->idCategoria= $Id;

        if (Input::hasPost('subcategoria')) {

        	$fechacreate= date('Y-m-d');
        	$fechaupdate= date('Y-m-d');
        	$idCategoria=$Id;
        	

        	$subcategoria = new Subcategoria(Input::post('subcategoria'));

        	$subcategoria->createdAt=$fechacreate;
        	$subcategoria->updatedAt=$fechaupdate;
        	$subcategoria->CategoriumId=$idCategoria;

           if($subcategoria->create()){
                Flash::valid('Operación exitosa');
                //Eliminamos el POST, si no queremos que se vean en el form
                Input::delete();
                return;               
            }else{
                Flash::error('Falló Operación');
            }

           //return Redirect::to();
        }
        // Sólo es necesario para el autoForm
        //$this->{$this->model} = new $this->model;
    }

    /**
     * Edita un Registro
     */
    public function editar($id,$CategoriumId)
    {

      $cat = new Categoria();
      $this->categoria = $cat->find_first("Id=".$CategoriumId,"columns: categoria.Id, categoria.nombre");
      //View::select('crear');
      $fechaActual=date('Y-m-d h:i:s');
      $subcategoria= new Subcategoria();
        if($id != null){
    	    //Aplicando la autocarga de objeto, para comenzar la edición
            $this->subcategoria = $subcategoria->find((int)$id);

            $subcategoria->updatedAt= $fechaActual;
    	}
        //se verifica si se ha enviado el formulario (submit)
        if(Input::hasPost('subcategoria')){
 
            if(!$subcategoria->update(Input::post('subcategoria'))){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->subcategoria = Input::post('subcategoria');
            } else {
               return Redirect::to('subcategoria/index/'.$CategoriumId);
            }
        }
    }

     /**
     * Borra un Registro
     */
    public function borrar($id,$cat)
    {
        if (!(new $this->model)->delete((int) $id)) {
            Flash::error('Falló Operación');
        }
        //enrutando al index para listar los articulos
        Redirect::to('subcategoria/index/'.$cat);
    }

     /**
     * Ver un Registro
     */
    public function ver($Id,$CategoriumId)
    {
    
    	$cat = new Categoria();
      $this->categoria = $cat->find_first("Id=".$CategoriumId,"columns: categoria.Id, categoria.nombre");

        $this->data = (new $this->model)->find_first((int) $Id);
    }

}