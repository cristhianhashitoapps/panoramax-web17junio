<?php
class DoctorsController extends ScaffoldController
{
 public $model = 'doctors';

  /**
     * Crea un Registro
     */
    public function crear()
    {

        if (Input::hasPost('doctors')) {

        	$fechacreate= date('Y-m-d');
        	$fechaupdate= date('Y-m-d');

        	$doctors = new Doctors(Input::post('doctors'));

        	$doctors->createdAt=$fechacreate;
        	$doctors->updatedAt=$fechaupdate;

           if($doctors->create()){
                Flash::valid('Operación exitosa');
                //Eliminamos el POST, si no queremos que se vean en el form
                Input::delete();
                return;               
            }else{
                Flash::error('Falló Operación');
            }

           //return Redirect::to();
        }
        // Sólo es necesario para el autoForm
        //$this->{$this->model} = new $this->model;
    }
    
     /**
     * Edita un Registro
     */
    public function editar($id)
    {
      //View::select('crear');
      $fechaActual=date('Y-m-d h:i:s');
      $doctors= new Doctors();
        if($id != null){
            //Aplicando la autocarga de objeto, para comenzar la edición
            $this->doctors = $doctors->find((int)$id);

            $doctors->updatedAt= $fechaActual;
        }
        //se verifica si se ha enviado el formulario (submit)
        if(Input::hasPost('doctors')){
 
            if(!$doctors->update(Input::post('doctors'))){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->doctors = Input::post('doctors');
            } else {
               return Redirect::to();
            }
        }
    }
}