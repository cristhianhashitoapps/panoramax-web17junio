
<?php
class PacientesController extends ScaffoldController
{
 public $model = 'pacientes';

 /**
     * Crea un Registro
     */
    public function crear()
    {

        if (Input::hasPost('pacientes')) {

        	$fechacreate= date('Y-m-d');
        	$fechaupdate= date('Y-m-d');

        	$pacientes = new Pacientes(Input::post('pacientes'));

        	$pacientes->createdAt=$fechacreate;
        	$pacientes->updatedAt=$fechaupdate;

           if($pacientes->create()){
                Flash::valid('Operación exitosa');
                //Eliminamos el POST, si no queremos que se vean en el form
                Input::delete();
                return;               
            }else{
                Flash::error('Falló Operación');
            }

           //return Redirect::to();
        }
        // Sólo es necesario para el autoForm
        //$this->{$this->model} = new $this->model;
    }
    
     /**
     * Edita un Registro
     */
    public function editar($id)
    {
      //View::select('crear');
      $fechaActual=date('Y-m-d h:i:s');
      $pacientes= new Pacientes();
        if($id != null){
            //Aplicando la autocarga de objeto, para comenzar la edición
            $this->pacientes = $pacientes->find((int)$id);

            $pacientes->updatedAt= $fechaActual;
        }
        //se verifica si se ha enviado el formulario (submit)
        if(Input::hasPost('pacientes')){
 
            if(!$pacientes->update(Input::post('pacientes'))){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->pacientes = Input::post('pacientes');
            } else {
               return Redirect::to();
            }
        }
    }
}