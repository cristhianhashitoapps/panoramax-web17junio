<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class IndexController extends AppController
{
	public $model = 'solicitudexamenes';
	public function index($page=1){
		$this->users = Load::model("usuariobacks")->find();
    	$this->arr = Load::model("doctors")->find();
        $this->data = (new Solicitudexamenes)->getsolicitudExamenes($page);
        $this->pedidosEstadoEnviado = 0;
        $this->pedidosEstadoAtendido = 0;
        $this->numeroUsuariosUltimoMes = 0;
    }
}