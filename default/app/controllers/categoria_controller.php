
<?php
class CategoriaController extends ScaffoldController
{
 public $model = 'categoria';
  public function index($page=1)
    {
        $this->data = (new Categoria)->getCategoria($page);
    }


    /**
     * Crea un Registro
     */
    public function crear()
    {

    	 $tipo = new Tiposervicios();
        $this->tiposervicios_id = $tipo->find("columns: tiposervicios.Id, tiposervicios.nombre");
  
        if (Input::hasPost('categoria')) {

        	$fechacreate= date('Y-m-d');
        	$fechaupdate= date('Y-m-d');

        	$categoria = new Categoria(Input::post('categoria'));

        	$categoria->createdAt=$fechacreate;
        	$categoria->updatedAt=$fechaupdate;

           if($categoria->create()){
                Flash::valid('Operación exitosa');
                //Eliminamos el POST, si no queremos que se vean en el form
                Input::delete();
                return;               
            }else{
                Flash::error('Falló Operación');
            }

           //return Redirect::to();
        }
        // Sólo es necesario para el autoForm
        //$this->{$this->model} = new $this->model;
    }


     /**
     * Edita un Registro
     */
    public function editar($id)
    {

        $tipo = new Tiposervicios();
        $this->tiposervicios_id = $tipo->find("columns: tiposervicios.Id, tiposervicios.nombre");
      //View::select('crear');
      $fechaActual=date('Y-m-d h:i:s');
      $categoria= new Categoria();
        if($id != null){
            //Aplicando la autocarga de objeto, para comenzar la edición
            $this->categoria = $categoria->find((int)$id);

            $categoria->updatedAt= $fechaActual;
        }
        //se verifica si se ha enviado el formulario (submit)
        if(Input::hasPost('categoria')){
 
            if(!$categoria->update(Input::post('categoria'))){
                Flash::error('Falló Operación');
                //se hacen persistente los datos en el formulario
                $this->categoria = Input::post('categoria');
            } else {
               return Redirect::to();
            }
        }
    }

}