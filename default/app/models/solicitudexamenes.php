<?php
class Solicitudexamenes extends ActiveRecord
{

	

	function getsolicitudExamenes($page) 
    {
        return $this->paginate('columns: solicitudexamenes.Id,solicitudexamenes.observaciones, solicitudexamenes.createdAt,solicitudexamenes.updatedAt,doc.nombres as DoctorId,doc.apellidos as DoctorApe, pac.nombres as PacienteId,pac.apellidos as PacienteApe,serv.nombre as TipoServicioId, est.estado as TipoEstadoId','join: left outer join doctors doc on solicitudexamenes.DoctorId = doc.Id left outer join pacientes pac on solicitudexamenes.PacienteId = pac.Id left outer join tiposervicios serv on solicitudexamenes.TipoServicioId = serv.Id left outer join tipoestados est on solicitudexamenes.TipoEstadoId = est.Id',"page: $page",'order: solicitudexamenes.Id desc');

       
    }
	
}