
<?php
class Categoria extends ActiveRecord
{

	function getCategoria($page) 
    {
        return $this->paginate('columns: categoria.Id, categoria.nombre, categoria.Imagen, categoria.createdAt, categoria.updatedAt, tipo.nombre as tiposervicios_id','join: left outer join tiposervicios tipo on categoria.tiposervicios_id = tipo.Id',"page: $page",
        'order: categoria.Id desc');
    }
}