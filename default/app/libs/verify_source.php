<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of verify_source
 *
 * @author Asus
 */
class VerifySource {

    //put your code here
    public static function verifyController($controller_name) {
        //verifica en esta lista de control
        $array = array("userservice");
        if (in_array($controller_name, $array)) {
            return true;
        } else {
            return false;
        }
    }

    public static function verifyAction($action_name) {
        //verifica en esta lista de control
        $array = array("post_token", "post_RequestSMS");
        if (in_array($action_name, $array)) {
            return true;
        } else {
            return false;
        }
    }

    public static function verifyToken($token) {
        $tok = substr($token, 9, strlen($token));
        //$tok= $token;

        $jwt = new \Emarref\Jwt\Jwt();
        $token = new \Emarref\Jwt\Token();
        $token = $jwt->deserialize($tok);
        $audience = $jwt->deserialize($tok)->getPayload()->findClaimByName(Emarref\Jwt\Claim\Audience::NAME)->getValue();
        $issuer = $jwt->deserialize($tok)->getPayload()->findClaimByName(Emarref\Jwt\Claim\Issuer::NAME)->getValue();
        $subject = $jwt->deserialize($tok)->getPayload()->findClaimByName(Emarref\Jwt\Claim\Subject::NAME)->getValue();

        $algorithm = new Emarref\Jwt\Algorithm\Hs256(VerifySource::getSecretKey());
        //$algorithm = new Emarref\Jwt\Algorithm\None();
        $encryption = Emarref\Jwt\Encryption\Factory::create($algorithm);

        $context = new Emarref\Jwt\Verification\Context($encryption);
        $context->setAudience($audience[0]);
        $context->setIssuer($issuer);
        $context->setSubject($subject);

        $verification = $jwt->verify($token, $context);
        return $verification;
    }

    public static function getSecretKey() {
        return "TUMENU2016*";
    }

    public static function getSocketServer(){
        return "http://www.masdomicilios.club:3005";
    }

}
