-- MySQL dump 10.13  Distrib 5.7.15, for Linux (x86_64)
--
-- Host: localhost    Database: pano1julio
-- ------------------------------------------------------
-- Server version	5.7.15-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) CHARACTER SET latin1 NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `Imagen` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `tiposervicios_id` int(11) DEFAULT NULL,
  `clase` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `tiposervicios_id_foreign_idx` (`tiposervicios_id`),
  CONSTRAINT `tiposervicios_id_foreign_idx` FOREIGN KEY (`tiposervicios_id`) REFERENCES `tiposervicios` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Ortopedia','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Estudios para Ortodoncia',1),(2,'Ortodoncia Básico','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Estudios para Ortodoncia',2),(3,'Sencillo','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Estudios para Ortodoncia',3),(4,'Personalizado','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Estudios para Ortodoncia',4),(5,'Empresarial','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Estudios para Ortodoncia',5),(6,'Quirúrgico','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',6),(7,'Restauración','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',7),(8,'Implantología','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',8),(9,'Diseño de sonrisa','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',9),(10,'Periodoncia','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',10),(11,'Periodoncia por sextantes','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',11),(12,'Tomografía','2017-03-18 00:00:00','2017-07-01 11:17:22','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',12),(13,'Fotografía','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',1,'Otros Estudios',13),(14,'Radiología intraoral','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',2,NULL,2),(15,'Radiologia extraoral','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',2,NULL,3),(16,'Análisis cefalométricos','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',2,NULL,4),(17,'Otros','2017-03-18 00:00:00','2017-03-18 00:00:00','http://localhost:3001/productos/panoramax.jpg',2,NULL,5),(18,'Tomografía','2017-06-19 00:00:00','2017-06-19 00:00:00',' http://www.masdomicilios.club:3005/productos/panoramax.jpg',2,NULL,1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallesexamenes`
--

DROP TABLE IF EXISTS `detallesexamenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallesexamenes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `campoAbierto` varchar(200) NOT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `SubCategoriumId` int(11) DEFAULT NULL,
  `SolicitudExameneId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `SubCategoriumId` (`SubCategoriumId`),
  KEY `SolicitudExameneId` (`SolicitudExameneId`),
  CONSTRAINT `detallesexamenes_ibfk_1` FOREIGN KEY (`SubCategoriumId`) REFERENCES `subcategoria` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `detallesexamenes_ibfk_2` FOREIGN KEY (`SolicitudExameneId`) REFERENCES `solicitudexamenes` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=420 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallesexamenes`
--

LOCK TABLES `detallesexamenes` WRITE;
/*!40000 ALTER TABLE `detallesexamenes` DISABLE KEYS */;
INSERT INTO `detallesexamenes` VALUES (347,'ninguno','2017-07-01 15:35:34','2017-07-01 15:35:34',1,80),(348,'ninguno','2017-07-01 15:35:34','2017-07-01 15:35:34',2,80),(349,'ninguno','2017-07-01 15:35:34','2017-07-01 15:35:34',3,80),(350,'ninguno','2017-07-01 15:35:34','2017-07-01 15:35:34',4,80),(351,'ninguno','2017-07-01 15:35:34','2017-07-01 15:35:34',6,80),(352,'ninguno','2017-07-01 15:35:34','2017-07-01 15:35:34',5,80),(353,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',1,81),(354,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',2,81),(355,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',3,81),(356,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',45,81),(357,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',44,81),(358,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',4,81),(359,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',6,81),(360,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',43,81),(361,'ninguno','2017-07-01 21:15:14','2017-07-01 21:15:14',5,81),(362,'ninguno','2017-07-01 21:34:08','2017-07-01 21:34:08',1,82),(363,'ninguno','2017-07-01 21:34:08','2017-07-01 21:34:08',2,82),(364,'ninguno','2017-07-01 21:34:08','2017-07-01 21:34:08',3,82),(365,'ninguno','2017-07-01 21:34:08','2017-07-01 21:34:08',4,82),(366,'ninguno','2017-07-01 21:34:08','2017-07-01 21:34:08',5,82),(367,'ninguno','2017-07-01 21:34:08','2017-07-01 21:34:08',6,82),(368,'ninguno','2017-07-01 22:06:19','2017-07-01 22:06:19',43,83),(369,'ninguno','2017-07-01 22:06:19','2017-07-01 22:06:19',44,83),(370,'ninguno','2017-07-01 22:06:19','2017-07-01 22:06:19',45,83),(371,'900800','2017-07-01 22:06:19','2017-07-01 22:06:19',49,83),(372,'ninguno','2017-07-01 22:17:58','2017-07-01 22:17:58',62,84),(373,'ninguno','2017-07-01 22:17:58','2017-07-01 22:17:58',60,84),(374,'dientes1','2017-07-01 22:17:58','2017-07-01 22:17:58',49,84),(375,'ninguno','2017-07-01 22:17:58','2017-07-01 22:17:58',56,84),(376,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',1,85),(377,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',2,85),(378,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',3,85),(379,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',4,85),(380,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',5,85),(381,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',6,85),(382,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',43,85),(383,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',44,85),(384,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',45,85),(385,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',46,85),(386,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',47,85),(387,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',48,85),(388,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',54,85),(389,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',55,85),(390,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',56,85),(391,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',58,85),(392,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',60,85),(393,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',59,85),(394,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',57,85),(395,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',61,85),(396,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',62,85),(397,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',63,85),(398,'ninguno','2017-07-01 22:47:27','2017-07-01 22:47:27',86,85),(399,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',54,86),(400,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',55,86),(401,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',56,86),(402,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',61,86),(403,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',57,86),(404,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',58,86),(405,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',60,86),(406,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',62,86),(407,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',86,86),(408,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',63,86),(409,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',59,86),(410,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',7,86),(411,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',8,86),(412,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',9,86),(413,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',10,86),(414,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',11,86),(415,'ninguno','2017-07-01 22:51:24','2017-07-01 22:51:24',12,86),(416,'ninguno','2017-07-01 23:49:26','2017-07-01 23:49:26',95,87),(417,'ninguno','2017-07-01 23:49:26','2017-07-01 23:49:26',96,87),(418,'ninguno','2017-07-01 23:49:26','2017-07-01 23:49:26',97,87),(419,'ninguno','2017-07-01 23:49:26','2017-07-01 23:49:26',98,87);
/*!40000 ALTER TABLE `detallesexamenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors`
--

DROP TABLE IF EXISTS `doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctors` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `clave` varchar(255) DEFAULT NULL,
  `correo` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `UserPushId` varchar(255) DEFAULT NULL,
  `DevicePushId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors`
--

LOCK TABLES `doctors` WRITE;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` VALUES (50,'Cristhian','Lombana','3105747269','JYN5IF','correo','2017-07-01 15:33:25','2017-07-01 15:34:29','698a435e-8dd3-4ecf-852a-68b115aa4f98','cPO9E1SrGpY:APA91bGnhV87NRaHPeciehoTwDY4hfDCQSbn877aSpAr3loIEJQT8m8-DSB3C91dVDf0fTiZiv_J4ZRvmPIQ8Vc31-gsSl75E9HisL20DFf_QQoh2sC8z5b4chPQ3p6BSBroPWYXfsAV'),(51,'adas','asdasd','3163619288','WQIZ4X','gedarufi@hotmail.com','2017-07-01 17:27:01','2017-07-01 17:27:01',NULL,NULL),(52,'carlos','lompera','tel','TEBLN3','cor','2017-07-01 21:13:34','2017-07-01 23:24:11','9818ffdc-81a2-4aa8-b01b-b016056c2e05','fgMBCTFI1CY:APA91bGJOa2eR-zwV3dPgBrvQDV5EQfvGTi_BNsLcImxaBaYvH5JKsM6NY7lM7pU7pxH7iCDjtj5k7C1A_4IkaYG5Q_mhDywaM6P5qx9pL1dQXsXIEquydUGmoMAabqxyfSFVL6AK3Z5'),(53,'paola','paz','300','9THMNP','correo','2017-07-01 21:33:17','2017-07-01 21:33:17',NULL,NULL),(54,'Juan David','Monroy','3144711546','GSNLBB','juanda327@hotmail.com','2017-07-02 17:16:50','2017-07-02 17:16:50',NULL,NULL),(55,'Juan David','Monroy','3144711536','F5Q1VF','juanda327@hotmail.com','2017-07-02 18:48:18','2017-07-02 18:48:59','6a13874a-2855-41a6-9cc1-fa77efc8a684','fpEyYR-Xt9w:APA91bGndgRXaT1KUpAkx-XmUmvAu0PDssAeDukjFIzb58Lq-GLdF5LfE6uKRu-WuDnjn766MPlf7zVkjClnxw5ux2dIcsrMVd6exDRmDN4UwoAYTlwl3gnIe0JHPGtlhf3K8nOIqFfI');
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresas`
--

DROP TABLE IF EXISTS `empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `tipoDireccion` varchar(255) NOT NULL,
  `Num1` varchar(255) NOT NULL,
  `Num2` varchar(255) NOT NULL,
  `Num3` varchar(255) NOT NULL,
  `Descripcion` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `DoctorId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `DoctorId` (`DoctorId`),
  CONSTRAINT `empresas_ibfk_1` FOREIGN KEY (`DoctorId`) REFERENCES `doctors` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresas`
--

LOCK TABLES `empresas` WRITE;
/*!40000 ALTER TABLE `empresas` DISABLE KEYS */;
INSERT INTO `empresas` VALUES (62,'consultorio','Carrera','7','8','8','des','2017-07-01 15:33:25','2017-07-01 15:33:25',50),(63,'asdasd','Calle','12','12','12','Calle 12','2017-07-01 17:27:01','2017-07-01 17:27:01',51),(64,'cnsu','Carrera','6','8','9','des','2017-07-01 21:13:35','2017-07-01 21:13:35',52),(65,'consultorio','Carrera','5','5','5','des','2017-07-01 21:33:17','2017-07-01 21:33:17',53),(66,'nombre','Carrera','7','8','9','descripción','2017-07-01 23:48:52','2017-07-01 23:48:52',52),(67,'Consultorio','Calle','49','9','21','Cons.','2017-07-02 18:48:18','2017-07-02 18:48:18',55);
/*!40000 ALTER TABLE `empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagens`
--

DROP TABLE IF EXISTS `imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `RutaGrande` varchar(255) NOT NULL,
  `RutaPequeÃ±o` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `CategoriumId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CategoriumId` (`CategoriumId`),
  CONSTRAINT `imagens_ibfk_1` FOREIGN KEY (`CategoriumId`) REFERENCES `categoria` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagens`
--

LOCK TABLES `imagens` WRITE;
/*!40000 ALTER TABLE `imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacientes`
--

DROP TABLE IF EXISTS `pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pacientes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(255) NOT NULL DEFAULT '',
  `apellidos` varchar(255) NOT NULL DEFAULT '',
  `telefono` varchar(10) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `cedula` int(11) NOT NULL,
  `fechaNacimiento` datetime NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacientes`
--

LOCK TABLES `pacientes` WRITE;
/*!40000 ALTER TABLE `pacientes` DISABLE KEYS */;
INSERT INTO `pacientes` VALUES (87,'Fernanda','Alvarez','300',45,'falvarez',123,'2017-07-21 05:00:00','2017-07-01 15:35:06','2017-07-01 15:35:06'),(88,'Luisaa','Arvele','344',3,'crisbs',828282,'2017-07-01 05:00:00','2017-07-01 21:14:50','2017-07-01 21:14:50'),(89,'carlo','paz','324324',3,'criferlo',32432,'2017-07-20 05:00:00','2017-07-01 21:33:54','2017-07-01 21:33:54'),(90,'jairo','bucheli','380',7,'criferlo',900,'2017-07-01 05:00:00','2017-07-01 22:05:58','2017-07-01 22:05:58'),(91,'Cecilisa','Alvarez','342343',233,'dsdsfds',3243243,'2017-07-27 05:00:00','2017-07-01 22:17:43','2017-07-01 22:17:43'),(92,'maria','perez','300',30,'cksksk',82828,'2017-07-01 05:00:00','2017-07-01 22:47:11','2017-07-01 22:47:11'),(93,'luis','chavez','324234',33,'crifer',324343,'2017-07-27 05:00:00','2017-07-01 22:51:11','2017-07-01 22:51:11'),(94,'camila','arcos','300',7,'ccc',888,'2017-07-01 05:00:00','2017-07-01 23:49:16','2017-07-01 23:49:16');
/*!40000 ALTER TABLE `pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sequelizemeta`
--

DROP TABLE IF EXISTS `sequelizemeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sequelizemeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `SequelizeMeta_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sequelizemeta`
--

LOCK TABLES `sequelizemeta` WRITE;
/*!40000 ALTER TABLE `sequelizemeta` DISABLE KEYS */;
INSERT INTO `sequelizemeta` VALUES ('20170421212445-unnamed-migration.js'),('20170421222922-unnamed-migration.js'),('20170422155642-unnamed-migration.js'),('20170424143904-unnamed-migration.js'),('20170424154238-unnamed-migration.js'),('20170424202603-unnamed-migration.js'),('20170628224843-unnamed-migration.js'),('20170629161055-unnamed-migration.js'),('20170629161542-unnamed-migration.js');
/*!40000 ALTER TABLE `sequelizemeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitudexamenes`
--

DROP TABLE IF EXISTS `solicitudexamenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitudexamenes` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `observaciones` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime NOT NULL,
  `DoctorId` int(11) DEFAULT NULL,
  `PacienteId` int(11) DEFAULT NULL,
  `TipoServicioId` int(11) DEFAULT NULL,
  `TipoEstadoId` int(11) DEFAULT NULL,
  `EmpresaId` int(11) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `DoctorId` (`DoctorId`),
  KEY `PacienteId` (`PacienteId`),
  KEY `TipoServicioId` (`TipoServicioId`),
  KEY `TipoEstadoId` (`TipoEstadoId`),
  KEY `empresa_id_foreign_idx` (`empresa_id`),
  CONSTRAINT `empresa_id_foreign_idx` FOREIGN KEY (`empresa_id`) REFERENCES `empresas` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `solicitudexamenes_ibfk_1` FOREIGN KEY (`DoctorId`) REFERENCES `doctors` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `solicitudexamenes_ibfk_2` FOREIGN KEY (`PacienteId`) REFERENCES `pacientes` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `solicitudexamenes_ibfk_3` FOREIGN KEY (`TipoServicioId`) REFERENCES `tiposervicios` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `solicitudexamenes_ibfk_4` FOREIGN KEY (`TipoEstadoId`) REFERENCES `tipoestados` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitudexamenes`
--

LOCK TABLES `solicitudexamenes` WRITE;
/*!40000 ALTER TABLE `solicitudexamenes` DISABLE KEYS */;
INSERT INTO `solicitudexamenes` VALUES (80,'observa','2017-07-01 15:35:33','2017-07-01 15:35:33',50,87,1,1,62,NULL),(81,'obs','2017-07-01 21:15:14','2017-07-01 21:15:14',52,88,1,1,64,NULL),(82,'observaciones','2017-07-01 21:34:08','2017-07-01 21:34:08',53,89,1,1,65,NULL),(83,'observaciones','2017-07-01 22:06:18','2017-07-01 22:06:18',52,90,1,1,64,NULL),(84,'observaciones','2017-07-01 22:17:57','2017-07-01 22:17:57',53,91,1,1,65,NULL),(85,'observaciones','2017-07-01 22:47:27','2017-07-01 22:47:27',52,92,1,1,64,NULL),(86,'observaciones','2017-07-01 22:51:24','2017-07-01 22:51:24',53,93,1,1,65,NULL),(87,'observaciones','2017-07-01 23:49:25','2017-07-01 23:49:25',52,94,2,1,66,NULL);
/*!40000 ALTER TABLE `solicitudexamenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitudtipoentregas`
--

DROP TABLE IF EXISTS `solicitudtipoentregas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitudtipoentregas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `SolicitudExameneId` int(11) DEFAULT NULL,
  `TipoEntregaResultadoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `SolicitudExameneId` (`SolicitudExameneId`),
  KEY `TipoEntregaResultadoId` (`TipoEntregaResultadoId`),
  CONSTRAINT `solicitudtipoentregas_ibfk_1` FOREIGN KEY (`SolicitudExameneId`) REFERENCES `solicitudexamenes` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `solicitudtipoentregas_ibfk_2` FOREIGN KEY (`TipoEntregaResultadoId`) REFERENCES `tipoentregaresultados` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitudtipoentregas`
--

LOCK TABLES `solicitudtipoentregas` WRITE;
/*!40000 ALTER TABLE `solicitudtipoentregas` DISABLE KEYS */;
INSERT INTO `solicitudtipoentregas` VALUES (61,'2017-07-01 15:35:34','2017-07-01 15:35:34',80,2),(62,'2017-07-01 15:35:34','2017-07-01 15:35:34',80,1),(63,'2017-07-01 21:15:14','2017-07-01 21:15:14',81,2),(64,'2017-07-01 21:15:14','2017-07-01 21:15:14',81,1),(65,'2017-07-01 21:34:09','2017-07-01 21:34:09',82,1),(66,'2017-07-01 21:34:09','2017-07-01 21:34:09',82,2),(67,'2017-07-01 22:06:19','2017-07-01 22:06:19',83,1),(68,'2017-07-01 22:06:19','2017-07-01 22:06:19',83,2),(69,'2017-07-01 22:17:58','2017-07-01 22:17:58',84,1),(70,'2017-07-01 22:47:27','2017-07-01 22:47:27',85,1),(71,'2017-07-01 22:47:27','2017-07-01 22:47:27',85,3),(72,'2017-07-01 22:51:25','2017-07-01 22:51:25',86,1),(73,'2017-07-01 22:51:25','2017-07-01 22:51:25',86,2),(74,'2017-07-01 23:49:26','2017-07-01 23:49:26',87,1);
/*!40000 ALTER TABLE `solicitudtipoentregas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategoria`
--

DROP TABLE IF EXISTS `subcategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subcategoria` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `clase` varchar(100) DEFAULT NULL,
  `campo` varchar(100) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `CategoriumId` int(11) DEFAULT NULL,
  `ImagenPrincipal` varchar(255) DEFAULT NULL,
  `CssClass` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `CategoriumId` (`CategoriumId`),
  CONSTRAINT `subcategoria_ibfk_1` FOREIGN KEY (`CategoriumId`) REFERENCES `categoria` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategoria`
--

LOCK TABLES `subcategoria` WRITE;
/*!40000 ALTER TABLE `subcategoria` DISABLE KEYS */;
INSERT INTO `subcategoria` VALUES (1,'Panorámica',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',1,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(2,'Perfil',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',1,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(3,'Análisis Cefalométrico',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',1,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(4,'Modelos de estudio',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',1,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(5,'2 Fotos extraorales',NULL,'none','2017-03-18 00:00:00','2017-06-19 04:14:34',1,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(6,'2 Fotos intraorales',NULL,'none','2017-03-18 00:00:00','2017-06-19 04:14:25',1,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(7,'Panorámica ',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',2,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(8,'Perfil',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',2,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(9,'Análisis Cefalométrico',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',2,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(10,'Modelos de estudio',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',2,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(11,'3 Fotos extraorales',NULL,'none','2017-03-18 00:00:00','2017-06-19 04:16:26',2,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(12,'5 Fotos extraorales',NULL,'none','2017-03-18 00:00:00','2017-06-19 04:16:17',2,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(13,'Panorámica',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',3,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(14,'Perfil',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',3,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(15,'Modelos de estudio',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',3,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(16,'Según acuerdo con el Doctor',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',4,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(17,'Según acuerdo con la Empresa',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',5,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(18,'Panorámica','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(19,'Perfil','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(20,'Posteroanterior','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(21,'Aná¡lisis cefalométrico','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(22,'Steiner y Grummons','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(23,'Modelos de estudio ','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(24,'Análisis de modelos ','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(25,'6 Fotos intraorales','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(26,'3 Fotos extraorales 1 a 1','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(27,'CD','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',6,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(28,'Juego Periapical','','none','2017-03-18 00:00:00','2017-03-18 00:00:00',7,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(29,'Modelos de estudio',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',7,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(30,'Modelos de trabajo',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',7,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(31,'2 Fotos extraorales',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',7,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(32,'5 Fotos intraorales',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',7,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(33,'Modelos de trabajo','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',8,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(34,'2 Fotos extraorales ','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',8,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(35,'5 Fotos intraorales','Tomografía','none','2017-03-18 00:00:00','2017-03-18 00:00:00',8,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(36,'Panorámica',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',9,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(37,'Juego periapical',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',9,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(38,'Modelos de Trabajo','8 Fotos como se indique','none','2017-03-18 00:00:00','2017-03-18 00:00:00',9,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(39,'Juego periapical ',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',10,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(40,'5 Fotos intraorales',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',10,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(41,'Juego periapical',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',11,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(42,'12 Fotos intraorales',NULL,'none','2017-03-18 00:00:00','2017-03-18 00:00:00',11,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(43,'Der','Maxilar Superior Región','check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-33'),(44,'Ant','Maxilar Superior Región','check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-33'),(45,'Izq','Maxilar Superior Región','check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-33'),(46,'Der','Maxilar Inferior Región','check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-33'),(47,'Ant','Maxilar Inferior Región','check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-33'),(48,'Izq','Maxilar Inferior Región','check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-33'),(49,'Zona Dientes',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(50,'Endodoncia diente',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(51,'Lectura (entrega 3 días)',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(52,'ATM',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(53,'Senos Maxilares',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',12,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(54,'Oclusión frente','Fotografía Intraoral','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(55,'Oclusión lateral','Fotografía Intraoral','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(56,'Sup','Arcos','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(57,'Inf','Arcos','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(58,'Overjet ','','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(59,'Overbite','','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(60,'Frente','Fotografía Extraoral','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(61,'Sonrisa','Fotografía Extraoral','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(62,'Der','Perfil','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(63,'Izq','Perfil','check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(64,'Periapical dientes',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(65,'Periapical milimetrada',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(66,'Zonas edéntulas',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(67,'Coronales',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(68,'1 Placa','Juego Periapical','check','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(69,'4 Placas','Juego Periapical','check','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(70,'Superior ','Milimetrado Oclusal','check','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(71,'Inferior','Milimetrado Oclusal','check','2017-03-18 00:00:00','2017-03-18 00:00:00',14,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(72,'Panorámica',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(73,'Perfil',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(74,'Posteroanterior',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(75,'Carpograma',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(76,'ATM Transcraneal',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(77,'ATM Técnica Pánorex',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(78,'Senos paranasales - Waters',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(79,'Senos paranasales - Pánorex',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(80,'Paquete de senos paranasales',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(81,'Der','Cuerpo del maxilar','check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(82,'Izq','Cuerpo del maxilar','check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(83,'Der','Rama del maxilar','check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(84,'Izq','Rama del maxilar','check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-50'),(85,'Submentón vértex',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',15,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(86,'¿Las desea en tamaño 1 a 1?',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',13,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(87,'Steiner',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(88,'Bimler',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(89,'Legan y Burston: duros ',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(90,'Legan y Burston: blandos',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(91,'McNamara',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(92,'A.P. Grummons',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(93,'Rickets',NULL,'check','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(94,'Otro',NULL,'texto','2017-03-18 00:00:00','2017-03-18 00:00:00',16,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(95,'Modelos de estudio','MODELOS','check','2017-03-18 00:00:00','2017-03-18 00:00:00',17,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(96,'Modelos de trabajo','MODELOS','check','2017-03-18 00:00:00','2017-03-18 00:00:00',17,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(97,'Análisis de modelos','MODELOS','check','2017-03-18 00:00:00','2017-03-18 00:00:00',17,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(98,'Historia Clínica','HISTORIA CLINICA','check','2017-03-18 00:00:00','2017-03-18 00:00:00',17,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(99,'Der','Maxilar Superior','check','2017-06-19 00:00:00','2017-06-19 04:21:54',18,' http://www.masdomicilios.club:3005/productos/panoramax.jpg',NULL),(100,'Ant','Maxilar Superior','check','2017-06-19 00:00:00','2017-06-19 04:21:44',18,' http://www.masdomicilios.club:3005/productos/panoramax.jpg',NULL),(101,'Izq','Maxilar Superior','check','2017-06-19 00:00:00','2017-06-19 04:21:38',18,' http://www.masdomicilios.club:3005/productos/panoramax.jpg',NULL),(102,'Der','Maxilar Inferior','check','2017-06-19 00:00:00','2017-06-19 04:21:30',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg',NULL),(103,'Ant','Maxilar Inferior','check','2017-06-19 00:00:00','2017-06-19 04:21:23',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg',NULL),(104,'Izq','Maxilar Inferior','check','2017-06-19 00:00:00','2017-06-19 04:21:16',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg',NULL),(105,'Zona Dientes',NULL,'texto','2017-06-19 00:00:00','2017-06-19 04:04:17',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(106,'Endodoncia diente',NULL,'texto','2017-06-19 00:00:00','2017-06-19 00:00:00',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(107,'Lectura (entrega 3 días)',NULL,'texto','2017-06-19 00:00:00','2017-06-19 00:00:00',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(108,'ATM',NULL,'texto','2017-06-19 00:00:00','2017-06-19 00:00:00',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100'),(109,'Senos Maxilares',NULL,'texto','2017-06-19 00:00:00','2017-06-19 00:00:00',18,'http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-100');
/*!40000 ALTER TABLE `subcategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoentregaresultados`
--

DROP TABLE IF EXISTS `tipoentregaresultados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoentregaresultados` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoentregaresultados`
--

LOCK TABLES `tipoentregaresultados` WRITE;
/*!40000 ALTER TABLE `tipoentregaresultados` DISABLE KEYS */;
INSERT INTO `tipoentregaresultados` VALUES (1,'Entregar al paciente','2017-06-17 00:00:00','2017-06-17 00:00:00'),(2,'Envío por correo electrónico','2017-06-17 00:00:00','2017-06-17 00:00:00'),(3,'Recibir copia en CD','2017-06-17 00:00:00','2017-06-17 00:00:00'),(4,'Envío a su consultorio','2017-06-17 00:00:00','2017-06-17 00:00:00');
/*!40000 ALTER TABLE `tipoentregaresultados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoestados`
--

DROP TABLE IF EXISTS `tipoestados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoestados` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoestados`
--

LOCK TABLES `tipoestados` WRITE;
/*!40000 ALTER TABLE `tipoestados` DISABLE KEYS */;
INSERT INTO `tipoestados` VALUES (1,'Enviado','2017-06-24 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `tipoestados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiposervicios`
--

DROP TABLE IF EXISTS `tiposervicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiposervicios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `Imagen` varchar(255) DEFAULT NULL,
  `CssClass` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiposervicios`
--

LOCK TABLES `tiposervicios` WRITE;
/*!40000 ALTER TABLE `tiposervicios` DISABLE KEYS */;
INSERT INTO `tiposervicios` VALUES (1,'Estudios de Ortodoncia y Cirugía','Escoja paquetes preestablecidos para ortodoncia, cirugía, implantología, entre otros','2017-04-23 00:00:00','0000-00-00 00:00:00','http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-25'),(2,'Radiología, Tomografía y otros servicios','Seleccione uno a uno los servicios que desee dentro de todas nuestras categorías','2017-04-23 00:00:00','0000-00-00 00:00:00','http://www.masdomicilios.club:3005/productos/panoramax.jpg','col-25');
/*!40000 ALTER TABLE `tiposervicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `identificacion` varchar(200) DEFAULT NULL,
  `clave` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2,'admin@gmail.com','admin@gmail.com','123');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariobacks`
--

DROP TABLE IF EXISTS `usuariobacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariobacks` (
  `EMAIL` varchar(255) NOT NULL DEFAULT '',
  `CLAVE` varchar(255) NOT NULL,
  `ENABLED` tinyint(1) NOT NULL DEFAULT '1',
  `INT_LOGIN` int(11) NOT NULL DEFAULT '0',
  `LAST_LOGIN` datetime DEFAULT NULL,
  `LOCKED` tinyint(1) DEFAULT NULL,
  `EXPIRED` tinyint(1) DEFAULT NULL,
  `EXPIRES_AT` datetime DEFAULT NULL,
  `CONFIRMATION_TOKEN` varchar(255) DEFAULT NULL,
  `PASSWORD_REQUESTED_AT` datetime DEFAULT NULL,
  `ROLES` text,
  `CREDENTIALS_EXPIRED` tinyint(1) DEFAULT NULL,
  `CREDENTIALS_EXPIRE_AT` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariobacks`
--

LOCK TABLES `usuariobacks` WRITE;
/*!40000 ALTER TABLE `usuariobacks` DISABLE KEYS */;
INSERT INTO `usuariobacks` VALUES ('criferlo@gmail.com','$2a$04$sQ0zGDIYE5fhNh1piaRPseizoJpaWrqEm1Bs5oGlbOX.PuDFb4lrW',0,0,'2017-05-10 03:39:41',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0000-00-00 00:00:00','2017-05-10 03:39:41');
/*!40000 ALTER TABLE `usuariobacks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-21 20:04:44
